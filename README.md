Hit-or-Wait Server
======

A flask server for hit-or-wait backend.

# Requirement
1. python 2.7+
2. [virtualenv](http://python-guide-pt-br.readthedocs.io/en/latest/dev/virtualenvs/)

# Local Development
1. Clone this repository and cd into it.
2. Create a new virtual environment with [virtualenv](https://virtualenv.pypa.io/en/latest/):
```
$ virtualenv venv
$ source venv/bin/activate
```
3. Install the requirements:
`$ pip install -r requirements.txt`
4. run flask app:
`$ python app.py&`
5. run Celery worker:
`$ celery -A app.celery worker -B --loglevel=info`

# Config
1. generate your own certificates for APNS. need link to a guide.


# API Endpoints
#
