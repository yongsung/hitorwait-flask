from __future__ import division
import flask
from flask import Flask, render_template, jsonify, request, Response
from how import HitorWait
import pprint
import json
from bson import json_util
from bson.son import SON
from bson.objectid import ObjectId

from pymongo import MongoClient, GEO2D
from roadAPI import toRoads, getRoutes, getMovementModel, postRoutesToDB, getRoadCoordinates, getRoad, get_nearby_roads

import io
import reverse_geocoder as rg
from logger import log

import time
from datetime import timedelta

from apns import APNs, Frame, Payload

import schedule

from push import scheduler

from pretracker import check_same_trip,get_search_region

from celery import Celery
from celery.schedules import crontab
import os

# # localhost
# uri = "mongodb://localhost:27017"
# ## for user testing
# dbName = "how"

## for simulation
# dbName = "myDB"

# need to put dbname in the mongdb uri!
# uri = "mongodb://yk:kim@ds011389.mlab.com:11389/otg"
# dbName = "otg"

## Heroku MONGODB URI
# uri = "mongodb://heroku_vv24r96s:h3poed8rejc1jojnco6jv9s68t@ds151018.mlab.com"
# port = 51018
# db = "heroku_vv24r96s"


app = Flask(__name__)
app.config.from_object('config')

if app.config['DEBUG']:
    ## localhost
    uri = "mongodb://localhost:27017"
    #for app
    dbName = "how"

    redis_uri = 'redis://localhost:6379/0'
else:
    # !!need to put dbname in the mongdb uri!!
    uri = "mongodb://yk:kim@ds011389.mlab.com:11389/otg"
    dbName = "otg"

    redis_uri = "redis://h:p79b00014880ea88a6b815af9ff80e5eac39fe904f54bea16a292d9e376856503@ec2-34-206-56-185.compute-1.amazonaws.com:28429"

# geocoder = rg.RGeocoder(mode=2, verbose=True, stream=io.open('il.csv',encoding='utf-8'))
print uri, dbName

client = MongoClient(uri)
db = client[dbName]

STEPS = 8
#TODO: change to redis url.
redis_uri = "redis://h:p79b00014880ea88a6b815af9ff80e5eac39fe904f54bea16a292d9e376856503@ec2-34-206-56-185.compute-1.amazonaws.com:28429"

app.config['CELERY_BROKER_URL'] = redis_uri
app.config['CELERY_RESULT_BACKEND'] = redis_uri

CELERYBEAT_SCHEDULE = {
    'every30': {
        'task': 'tasks.sendPush',
        # 'schedule': crontab(minute='*/1')
        'schedule': timedelta(seconds=30)
    },
}

# celery = Celery(app.name, broker=os.environ['REDIS_URL'])

celery = Celery(app.name, broker=redis_uri)

# celery.conf.update(BROKER_URL=os.environ['REDIS_URL'],
#                 CELERY_RESULT_BACKEND=os.environ['REDIS_URL'])

celery.conf.update(app.config)

# send push notifications
@app.route('/push')
def sendNotification():
    # prod
    # apns = APNs(use_sandbox=False, cert_file='otgCert.pem', key_file='otgKey.pem')

    # dev
    msg = request.args.get('msg',"",type=str)

    users = db.users

    result = users.find()
    print result.count()
    for r in result:
    # print "sending notification to: %s"%r["tokenId"]
        apns = APNs(use_sandbox=False, cert_file='otgCert.pem', key_file='otgKey.pem')
        token_hex = r["tokenId"]
        # msg = "Hello %s"%r["user"]
        payload = Payload(alert=msg, sound="default", badge=1, content_available=1,custom={"custom":"item"})
        apns.gateway_server.send_notification(token_hex, payload)
    return "sent"

@app.route('/')
def index():
    return "Hit-or-Wait Example"

@app.route('/example')
def example():
    search_region = ['s1','s2','s3','s4']

    maxTime = 3 # tree depth
    models = {"s1":{"s2":0.5},
             "s2":{"s1":0,"s3":1},
             "s3":{"s2":0.5,"s4":0.5},
             "s4":{"s3":1}}
    values = {"s1":10.0,"s2":30.0,"s3":5.0,"s4":20.0}
    hitorwait = HitorWait(search_region)
    hitorwait.addSearchRegionDown(search_region)
    hitorwait.setSearchValue(values)

    routes = ['s1','s2','s3','s4']
    decisions = hitorwait.computeWaitingDecisionTable(models,maxTime)
    # get (state,decision point)
    results = hitorwait.requestSearchHitorWait(routes,decisions)

    # models = json.dumps(models, sort_keys = True, indent = 4)
    # values = json.dumps(values, sort_keys = True)
    maxTime = len(decisions["decisions"])-1
    return render_template('index.html',maxTime=maxTime,
        decisions=decisions["decisions"],models=models,
        values=values,routes=routes,
        results=results)

@app.route('/run')
def runHitorWait():
    search_region = ['s1','s2','s3','s4']

    maxTime = 3 # tree depth
    # hitorwait.addSearches({"s1":2,"s2":0,"s3":2,"s4":1})
    # test 1
    model = {"s1":{"s2":0.5},
             "s2":{"s1":0,"s3":1},
             "s3":{"s2":0.5,"s4":0.5},
             "s4":{"s3":1}}
    search_value = {"s1":10.0,"s2":30.0,"s3":5.0,"s4":20.0}
    hitorwait = HitorWait(search_region)
    hitorwait.addSearchRegionDown(search_region)
    hitorwait.setSearchValue(search_value)

    # # THIS IS SO COOL!!!!!!!!!

    decisions = hitorwait.computeWaitingDecisionTable(model,maxTime)
    return decisions
    # return json.dumps(decisions,default=json_util.default)
    # return flask.jsonify(decisions)

# get user current location
# and store location to db, check if it is near search region, get hitorwait decisions
@app.route('/currentlocation', methods=['POST'])
def currentLocation():
    # user = request.args.get('user',"",type=str)
    # lat = request.args.get('lat',0.0,type=float)
    # lon = request.args.get('lon',0.0,type=float)
    # date = request.args.get('date',0,type=int)

    # using json format now.
    content = request.get_json(silent=True)
    user = content["user"]
    lat = content["lat"]
    lon = content["lon"]
    date = content["date"]

    date = int(time.time())

    check_same_trip(user,lat,lon,date)

    return jsonify(result="success")

# store user data to user collection
@app.route('/user')
def user():
    tokenId = request.args.get('tokenId',"",type=str)
    username = request.args.get('username',"",type=str)
    print tokenId, username

    users = db.users

    user_info = {}

    user_info["user"] = username
    user_info["tokenId"] = tokenId
    user_info["created"] = int(time.time())
    user_info["lastModified"] = int(time.time())

    uid = users.insert(user_info)
    if uid!=None:
        print uid
        return jsonify(result="success")

@app.route('/routes/<user>')
def routes(user):
    locations = db.locations
    locs = locations.find({"user":user})
    # return jsonify(lat="42.131",lng="-86.1231",user="yk")
    # print jsonify(locs[0])
    return Response(
        json_util.dumps(locs[0]),
        mimetype='application/json'
    )
    # return jsonify(result=locs[0])

# update search for the search region
@app.route('/updateSearch', methods=['POST'])
def updateSearch():
    # user = request.args.get('user','',type=str)
    # road = request.args.get('road',"",type=str)
    # lat = request.args.get('lat',"",type=str)
    # lon = request.args.get('lon',"",type=str)

    # uid = request.args.get('uid',"",type=str)
    # found = request.args.get('found',"0",type=str)

    content = request.get_json(silent=True)
    user = content['user']
    lat = content['lat']
    lon = content['lon']

    uid = content['uid']
    found = content['found']

    # getRoad function take [lat,lon] list as input
    # and output a list with one element in it. so index should be 0.
    # and the road name is the value for the key "name".
    road = getRoad([(float(lat),float(lon))])[0]["name"]
    key = "nearby_regions.%s"%road
    # query_str = {key:{"$exists":"true"}}

    # query_str = {"loc":SON({"$near":{"$geometry":{"type":"Point", "coordinates":[-87.678085,42.049423]},"$maxDistance":1000}})}
    query_str = {"_id":ObjectId(uid)}
    regions = db.regions
    result = regions.find(query_str)
    if result.count() > 0:
        for r in result:
    #         print r
    #         uid = r["_id"]
    #         client = MongoClient(uri)
    #         db = client[dbName]
    #         regions = db.regions
    #         indiv_result = regions.find({"_id":ObjectId(uid)})
            search_count = 0
        #         for indiv_res in indiv_result:
            search_count = r["nearby_regions"][road]
            search_count += 1
            helper_list = r["helpers"]
            helper_list.append(user)
            regions.update({"_id":ObjectId(uid)},{"$set":{key:search_count,"found":found,"helpers":helper_list}})
            return jsonify(result='success')
    else:
        return jsonify(result="failed")

@app.route('/getRegions/<region>')
def getRegions(region):
    # region = request.args.get('region',"",type=str)
    regions = db.regions
    print region
    query_result = regions.find({"region":region})
    # for query in query_result:
    #     pass
    if query_result.count()>0:
        # print query_result[0]
        # ways to return mongodb query results with Optional() in it.
        result = {}
        result = query_result[0]
        # return jsonify(result=query_result[0])
        # result["user"] = query_result[0]["user"]
        # result["user"] = "yk"
        # result["region"] = query_result[0]["region"]
        # result["nearby_regions"] = query_result[0]["nearby_regions"]
        # return jsonify(result)
        return Response(
            json_util.dumps(result),
            mimetype='application/json'
        )
    else:
        return jsonify({"result":"no data"})

# return nearby search regions based on the predefined distance
@app.route('/getNearbySearchRegion')
def getNearbySearchRegion():
    lat = request.args.get('lat',"",type=str)
    lon = request.args.get('lon',"",type=str)
    if "Optional" in lat:
        lat,lon = float(lat.split("(")[1].split(")")[0]), float(lon.split("(")[1].split(")")[0])
        print lat,lon
    else:
        lat,lon = float(lat), float(lon)

    regions = db.regions

    # in meters
    dist = 300

    query_str = {"loc":SON({"$near":{"$geometry":{"type":"Point", "coordinates":[lon,lat]},"$maxDistance":dist}}),"found":False}
    result = regions.find(query_str).limit(1)

    if result.count() > 0:
        for r in result:
            print r
            return Response(
                json_util.dumps(r),
                mimetype='application/json'
            )
    else:
        return jsonify({"result":0})

# request a lost item
@app.route('/regions',methods=["POST"])
def regions():
    try:
        content = request.get_json(silent=True)

        # username = request.form["user"]
        # # region = request.form["region"]
        # item = request.form["item"]
        # detail = request.form["detail"]
        # lat = request.form["lat"]
        # lng = request.form["lng"]
        # region = request.form["region"]

        username = content["user"]
        item = content["item"]
        detail = content["detail"]
        lat = content["lat"]
        lon = content["lon"]

        # username = content["user"]
        # routeId = content["routeId"]
        # routes = content["coordinates"]
        # print username
        region = getRoad([(lat,lon)])

        nearby_regions = get_nearby_roads(float(lat),float(lon))
        tmp_regions = {}
        roads_coordinates = {}
        for nearby_region in nearby_regions["roads"]:
            tmp_regions[nearby_region] = 0
            idx = nearby_regions["roads"].index(nearby_region)
            roads_coordinates[nearby_region] = nearby_regions["roads_coordinates"][idx]

        content = {"user":username, "item": item, "detail": detail, "region":region[0]["name"], "loc": {"type": "Point", "coordinates": [float(lon), float(lat)]}, "helpers":[], "found": False, "nearby_regions": tmp_regions, "roads_coordinates": roads_coordinates}

        regions = db.regions
        uid = None
        uid = regions.insert(content)
        if uid != None:
            print uid
            # nearby_regions = get_nearby_roads(float(lat),float(lon))
            # tmp_regions = {}
            # roads_coordinates = {}
            # for nearby_region in nearby_regions["roads"]:
            #     tmp_regions[nearby_region] = 0
            #     idx = nearby_regions["roads"].index(nearby_region)
            #     roads_coordinates[nearby_region] = nearby_regions["roads_coordinates"][idx]
            # regions.update({"_id":uid},{"$set":{"found":False, "helpers":[],"nearby_regions":tmp_regions,"roads_coordinates":roads_coordinates}})
            return jsonify(result="success")
        else:
            return jsonify(result="failed")
    except Exception as error:
        print error

@app.route('/how')
def getDecision():
    try:
        user = request.args.get('user','yk',type=str)
        road = request.args.get('road',"",type=str)
        lat = request.args.get('lat',"",type=str)
        lon = request.args.get('lon',"",type=str)

        # stupid code for swift Optional value. just in case I am careless on ios client side
        if "Optional" in lat:
            lat,lon = float(lat.split("(")[1].split(")")[0]), float(lon.split("(")[1].split(")")[0])
            print lat,lon
        # regions = get_nearby_roads(lat,lon)
        road = getRoad([(lat,lon)])
        road = road[0]["name"]
        print road

        # calculate 8 steps in total. 7 steps look ahead
        steps = 8
        models = getMovementModel(user,road,steps)
        pprint.pprint(dict(models))

        '''
        TODO:
        need logic for choosing individual- or population-based model.
        e.g., if model is empty, use population-based model
        '''

        roads = []
        roadsCoordinates = {}
        for i,v in models.iteritems():
            if i not in roads:
                roads.append(i)
                key,value = getRoadCoordinates(i)
                roadsCoordinates[key] = value
            for j in v.keys():
                if j not in roads:
                    roads.append(j)
                    key,value = getRoadCoordinates(j)
                    roadsCoordinates[key] = value

        # for i in range(0,len(regions["roads"])):
        #     roadsCoordinates[regions["roads"][i]] = regions["roads_coordinates"][i]
        # need to get nearby coordinates and roads

        # print "roads are: "
        # print roads
        # print roadsCoordinates
        # values = {'616-600 Church St':0.0,
        #     u'1798-1700 Chicago Ave':100,
        #     u'598-522 Church St': 50,
        #     u'520-500 Church St': 50,
        #     '698-618 Church St': 500}
        # search_region = ['616-600 Church St','1798-1700 Chicago Ave','598-522 Church St','520-500 Church St','698-618 Church St']
        # search_region = roads

        # need to get correct uid
        # uid = ObjectId("58a72007235b38cb25602c04")

        regions = db.regions
        values = {}
        res = regions.find({"_id":uid})
        for r in res:
            road_list = roads + r["nearby_regions"].keys()
            print road_list
            hitorwait = HitorWait(road_list)
            hitorwait.addSearchRegionDown(roads)
            hitorwait.setSearchCount(r["nearby_regions"])

            decisions = hitorwait.computeWaitingDecisionTable(models,steps)
            # need all the coordinates here.
            decisions.update({"coordinates":roadsCoordinates})
            decisions.update({"models":models})
            pprint.pprint(decisions)
        # pprint.pprint(decisions)
        #TODO: should return hit or wait decision and lat lng for each search region

        return jsonify(decisions)
    except Exception as error:
        print "in func getDecision"
        print error

@app.route('/models')
def getModel():
    # TODO: add to db
    # need user's most recent road
    '''
    input: user, current road, steps lookahead, type of movement model (idv or pop), include heading (0/1)
    output: movement model
    '''
    try:
        user = request.args.get('user','yk',type=str)
        road = request.args.get('road',"1629 Chicago Avenue",type=str)
        steps = request.args.get('steps',8,type=int)
        model = request.args.get('model',"pop",type=str)
        heading = request.args.get('heading',0,type=int)
        movmodel = getMovementModel(user,road,steps,model,heading)
        # pprint.pprint(dict(movmodel))
        # return Response(
        #         json_util.dumps(movmodel),
        #         mimetype='application/json'
        #     )
        return jsonify(result="success")
    except Exception as error:
        print "in getModel"
        print error

# return road segment name based on geocoordinate.
@app.route('/currentroad', methods=['POST'])
def getCurrentRoad():
    # lat = request.args.get('lat',0.0,type=float)
    # lng = request.args.get('lng',0.0,type=float)

    #using json now.
    content = request.get_json(silent=True)
    lat = content["lat"]
    lon = content["lon"]

    coordinate = []
    coordinate.append((lat,lon))
    result = getRoad(coordinate)
    road = result[0]["name"]
    print lat,lon,road
    return jsonify(road=road)

@app.route('/postRoutes',methods=['POST'])
def postRoutes():
    try:
        content = request.get_json(silent=True)
        print content
        username = content["user"]
        routeId = content["routeId"]
        routes = content["coordinates"]
        content["created"] = int(time.time())
        content["lastModified"] = int(time.time())

        locations = db.locations
        uid = None
        uid = locations.insert(content)
        if uid!=None:
            postRoutesToDB(username,routeId)
        print uid
        # username = request.args.get('user','')
        # route = request.args.get('route','')

        # response = {}
        # response["username"] = username
        # response["coordinates"] = route
        # print username, route
        response = jsonify(result="success")
        return response
    except Exception as error:
        print error

# setup period tasks with celery
@celery.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(30.0, sendPush.s(), name='every30')

    # Executes every Monday morning at 7:30 a.m.
    # sender.add_periodic_task(
    #     crontab(hour=7, minute=30, day_of_week=1),
    #     test.s('Happy Mondays!'),
    # )

# send silent push notification
@celery.task
def sendPush():
    users = db.users
    result = users.find()
    # print result.count()
    for r in result:
        apns = APNs(use_sandbox=False, cert_file='otgCert.pem', key_file='otgKey.pem')
        token_hex = r["tokenId"]
        payload = Payload(content_available=1)
        # payload = Payload(content_available=1)
        apns.gateway_server.send_notification(token_hex, payload)


if __name__ == "__main__":
    # task = sendPush.delay()
    # task = setup_periodic_tasks()
    app.run(host='0.0.0.0')




