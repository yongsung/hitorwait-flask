from __future__ import division
import json
import os
import pymongo
from pymongo import MongoClient, GEO2D
import time
from collections import defaultdict, Counter
from roadAPI import getRoad, get_direction, calculate_bearing, haversine, postRoutesToDB, getNearbyCoordinates,getMovementModel
from bson.son import SON
from sets import Set
import folium
import webbrowser
import random
import re
import pprint
import sys
from how import HitorWait
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

most_common_usr = []

cwd = os.getcwd()
cwd = cwd+"/exp/route/"

uri = "mongodb://localhost:27017"
dbName = "simulation"

#{"des": "health center", "id":"59","name":"Searle Hall","the_points":"42.0523598210687,-87.6794761419296|42.0523598210687,-87.6787948608398|42.0522263817999,-87.6787948608398|42.0522243901669,-87.6794922351837|42.0523598210687,-87.6794761419296","type":"1","centerpoint":"42.052292342679,-87.6791403813999","wifi":"Throughout the building"}},{"Facility":


# Chicago Suburb
# TOP = 42.130594
# LEFT = -87.836293
# BOTTOM = 42.019236
# RIGHT = -87.662696

# Evanston
TOP = 42.068926
LEFT = -87.703823

BOTTOM = 42.014010
RIGHT = -87.661586

cnt = 0

dist_filter = 30.0
def get_runkeeper_data(filename):
    # f_out_file = "exp/out/"+filename + ".out"
    # f_out = open(f_out_file,'w')
    map_data = []
    file_path = "exp/route/"+filename
    out_data = defaultdict(list)
    print file_path
    prev_lat, prev_lng = 0.0,0.0
    curr_lat,curr_lng = prev_lat,prev_lng
    with open(file_path,'r') as data_file:
        #json.LOAD for files
        data = json.load(data_file)
        for i in range(len(data)):
            curr_lat = data[i]["latitude"]
            curr_lng = data[i]["longitude"]
            if i==0:
                prev_lat,prev_lng = curr_lat,curr_lng
                map_data.append((curr_lat,curr_lng))
            else:
                dist = haversine(prev_lat,prev_lng,curr_lat,curr_lng)
                # print dist,
                if dist >= dist_filter:
                    map_data.append((curr_lat,curr_lng))
                    prev_lat,prev_lng = curr_lat,curr_lng
                else:
                    pass
                    # print "skipping"
        # print map_data
        tmp = filename.split("_")
        user = tmp[0]
        route_num = tmp[1].split(".json")[0]
        add_locations(user,map_data,route_num)
        # roads = getRoad(map_data)
        # for road in roads:
        #     print road["name"],

    # with open(f_out_file, 'w') as outfile:
    #     #{"route": "1521261", "the_points": "41.932365,-87.675707|41.932269,-87.678196|41.939644,-87.678325|41.94689,-87.678539|41.946986,-87.66884|41.925149,-87.668154|41.925149,-87.669184|41.932205,-87.678196|41.932205,-87.675707", "user": "rachelnewtoff"}

    #     tmp = filename.split("_")
    #     user = tmp[0]
    #     route_num = tmp[1].split(".json")[0]
    #     out_data["user"] = user
    #     out_data["route"] = route_num
    #     out_data["the_points"] = map_data
    #     if trimmed_data.endswith("|"):
    #         trimmed_data = trimmed_data[:-1]
    #     out_data["trimmed_points"] = trimmed_data
    #     json.dump(out_data, outfile)

# get_runkeeper_data()
def check_evanston(filename):
    file_path = "exp/route/"+filename
    with open(file_path) as data_file:
        #json.LOAD for files
        data = json.load(data_file)
        for i in range(len(data)):
            curr_lat = data[i]["latitude"]
            curr_lng = data[i]["longitude"]
            if curr_lat <= TOP and curr_lat >= BOTTOM and curr_lng >= LEFT and curr_lng <= RIGHT:
                return True
            else:
                return False

def add_locations(user,coordinates,routeId):
    content = {}
    content["user"] = user
    content["coordinates"] = coordinates
    routeId = user+routeId
    content["routeId"] = routeId
    # username = content["user"]
    # routeId = content["routeId"]
    # routes = content["coordinates"]
    # print username
    client = MongoClient(uri)
    db = client[dbName]
    locations = db.locations
    uid = None
    uid = locations.insert(content)
    if uid!=None:
        postRoutesToDB(user,routeId)
    # print uid

# cnt = 0
# evanston_list = []
# for file in os.listdir(cwd):
#     # print file
#     # file = "bbayliff_2377371.json"
#     get_runkeeper_data(file)

# 530 chicago suburb routes
# 261 evanston routes
# print cnt
# print evanston_list


def save_data():
    # cnt = 0
    with open('evanston_list.json','r') as json_file:
        evanston_json = json.load(json_file)
        evanston_routes = evanston_json["results"]
        for route in evanston_routes:
            get_runkeeper_data(route)

                # tmp = route.split("_")
                # user = tmp[0]
                # route_num = tmp[1].split(".json")[0]


# save_data()

def get_evanston_users(search_region):
    client = MongoClient(uri)
    db = client.myDB
    dist = 0.5/6378.1
    users = []
    user_color = {}
    user_set = Set()
    route_list = []
    tmp_regex = r'%s'%search_region
    regex = re.compile(tmp_regex)
    # query = {"roads":regex,"user":"800705664"}
    query = {"roads":regex}

    # query = {"locs":{"$geoWithin":{"$centerSphere":[[42.039504, -87.685167],dist]}}}
    # query = {"locs":{"$near":{"$geometry":{"type":"Point","coordinates":[-87.681640,42.058438]},"$maxDistance":2000,"$minDistance":0.0}}}
    # color = ""
    # user_road = defaultdict(dict)
    query_result = db.routes.find(query)
    cnt = 0
    for doc in query_result:
        user = doc["user"]
        user_set.add(user)
        # print pprint.pprint(dict(getMovementModel(doc["user"],"617 Noyes Street",3)))
        cnt+=1
    # print "%d users in total had passed this search region"%cnt
    return user_set


# east, west, south, north
#['617 Noyes Street', '624 Noyes Street', '616 Noyes Street', '617 Noyes Street',
# '617 Noyes Street', '629 Noyes Street', '617 Haven Street', '624 Dartmouth Place']

def get_nearby_roads(lat,lon):
    # 42.058372, -87.678015
    # print getRoad([(42.058372, -87.678015)])
    dist = 0.040
    coords = getNearbyCoordinates(lat,lon,0,dist)

    result = getRoad(coords)
    roads = []
    coordinates = []
    for r in result:
        # print r["name"], r["lat"], r["lon"]
        lat,lon = float(r["lat"]),float(r["lon"])
        if r["name"] not in roads:
            roads.append(r["name"])
            coordinates.append((lat,lon))
    # print roads
    # print coordinates

    regions = {}
    search_region = getRoad([(42.058372, -87.678015)])[0]
    regions["coordinate"] = (42.058372, -87.678015)
    regions["name"] = search_region["name"]
    regions["user"] = "username"
    regions["roads"] = roads
    regions["roads_coordinates"] = coordinates

    # client = MongoClient(uri)
    # db = client.myDB
    # rid = db.regions.insert(regions)
    # print rid

    return regions
    # for road in roads:
    #     road_set.add(road)
    # print road_set
    #adjacent roads --> add to search roads.

# get_nearby_roads()

def simulate(lat,lon):
    # need to choose notification policy
    # need to choose a route
    # choose location
    # get evanston users who passed by location x

    client = MongoClient(uri)
    db = client[dbName]

    #search region latitude and longitude
    # lat,lon = 42.058372, -87.678015 #noyes
    # lat,lon = 42.050597, -87.681911 #elgin
    # 42.048587, -87.676577 index out of range

    lat,lon = lat,lon

    # tuning steps and notification distance
    # road segments to lookahead
    steps = 5
    #this is in meters
    noti_distance = 140.0

    prev_history = 3

    search_region = getRoad([(lat,lon)])[0]["name"]
    regions = get_nearby_roads(lat,lon)

    #with search_region first
    evanston_user = get_evanston_users(search_region)
    tmp_regex = r'%s'%search_region
    regex = re.compile(tmp_regex)

    """
    hit or wait init
    """
    hitorwait = HitorWait(regions["roads"])
    hitorwait.addSearchRegionDown(regions["roads"])

    lost_item_idx = random.randint(0,len(regions["roads"])-1)

    print "lost item is at: %s" % regions["roads"][lost_item_idx]
    hitorwait.markLostItem(regions["roads"][lost_item_idx])

    search_median = HitorWait(regions["roads"])
    search_median.addSearchRegionDown(regions["roads"])
    search_median.markLostItem(regions["roads"][lost_item_idx])

    search_lowest = HitorWait(regions["roads"])
    search_lowest.addSearchRegionDown(regions["roads"])
    search_lowest.markLostItem(regions["roads"][lost_item_idx])

    search_opt = HitorWait(regions["roads"])
    search_opt.addSearchRegionDown(regions["roads"])

    how_dict = {}
    md_dict = {}
    low_dict = {}
    opt_dict = {}

    user_passed_dict = {}

    user_hist_routes = []

    for user in evanston_user:
        # print user
        query = {"roads":regex,"user":user}
        query_result = db.routes.find(query)
        if query_result.count()> prev_history:
            query_len = query_result.count()
            user_hist_routes.append(query_len)
            # random_route_num = random.randint(0,query_len-1)
            # # print "query count: %d, random index: %d"%(query_len,random_route_num)
            # result = query_result[random_route_num]
            for result in query_result:
                for coord in result["coordinates"]:
                    if haversine(coord[0],coord[1],lat,lon) <= noti_distance:
                        idx = result["coordinates"].index(coord)
                        # print result["roads"][idx-1],result["roads"][idx],result["roads"][idx+1]
                        user_path = []
                        if idx + steps >= len(result["roads"]):
                            continue

                        user_path.append(result["roads"][idx])
                        for i in range(1,steps):
                            # print user_path, user_path[-1]
                            if user_path[-1] != result["roads"][i+idx]:
                                user_path.append(result["roads"][i+idx])
                            # user_path.append(result["roads"][i+idx])
                        currentRoad = result["roads"][idx]
                        model = getMovementModel(user,currentRoad,steps)
                        print user

                        # print model
                        # print user_path
                        # return
                        # need to handle empty model
                        if model != None:
                            decisions = hitorwait.computeWaitingDecisionTable(model, steps)

                            # calculate how many times users passed the region from user_path

                            for road in user_path:
                                print road
                                if road in regions["roads"]:
                                    if road not in user_passed_dict:
                                        user_passed_dict[road] = 1
                                    else:
                                        user_passed_dict[road] += 1

                            #TODO: need to handle empty decisions. WHY returning empty decision?
                            if decisions != None:
                                pprint.pprint(decisions,indent=4)
                                hitorwait.requestSearchHitorWait(user_path,decisions)
                                search_median.requestSearchMedian(user_path)
                                search_lowest.requestSearchLowest(user_path)
                                search_opt.requestSearchOPT(user_path)
                                # if policy == "how":
                                #     hitorwait.requestSearchHitorWait(user_path,decisions)
                                # elif policy == "median":
                                #     hitorwait.requestSearchMedian(user_path)
                                # elif policy == "lowest":
                                #     hitorwait.requestSearchLowest(user_path)
                                # elif policy=="opt":
                                #     hitorwait.requestSearchOPT(user_path)
                                # else:
                                #     pass

                        break
        else:
            pass
        # break


    print "search hit or wait"
    print "search value is %f" % hitorwait.computeSearchValue(hitorwait.expDecaySum)
    print "total search count is %d" % hitorwait.getTotalSearchCounts()
    print hitorwait.printSearchCoverage()
    how_dict["search_count"] = hitorwait.getTotalSearchCounts()

    lost_item_how = []
    total_search_how = []

    lost_item_how.append(hitorwait.itemFoundCount)
    total_search_how.append(hitorwait.getTotalSearchCounts())

    print "search areas below median search attempts"
    print "search value is %f" % search_median.computeSearchValue(hitorwait.expDecaySum)
    print "total search count is %d" % search_median.getTotalSearchCounts()
    print search_median.printSearchCoverage()
    md_dict["search_count"] = search_median.getTotalSearchCounts()

    lost_item_median = []
    total_search_median = []
    lost_item_median.append(search_median.itemFoundCount)
    total_search_median.append(search_median.getTotalSearchCounts())

    print "search areas with lowest search attempts"
    print "search value is %f" % search_lowest.computeSearchValue(hitorwait.expDecaySum)
    print "total search count is %d" % search_lowest.getTotalSearchCounts()
    print search_lowest.printSearchCoverage()
    low_dict["search_count"] = search_lowest.getTotalSearchCounts()

    lost_item_lowest = []
    total_search_lowest = []

    lost_item_lowest.append(search_lowest.itemFoundCount)
    total_search_lowest.append(search_lowest.getTotalSearchCounts())

    print "search areas with optimal search attempts"
    print "search value is %f" % search_opt.computeSearchValue(hitorwait.expDecaySum)
    print "total search count is %d" % search_opt.getTotalSearchCounts()
    print "lost item found: %d" % search_opt.itemFoundCount

    print search_opt.printSearchCoverage()
    opt_dict["search_count"] = search_opt.getTotalSearchCounts()

    print "======== user passed regions ======= "
    print user_passed_dict
    print user_hist_routes
    # print "average user route history is: %f, median: %f, max: %f" % \
    #     (np.average(user_hist_routes),np.median(user_hist_routes),max(user_hist_routes))

    # print "# of users: %f" % len(evanston_user)

    policies = ["Hit-or-Wait","Median","Lowest"]
    params = []
    how_data = []
    # how_data.append(hitorwait.getTotalSearchCounts())
    how_values = {}
    for road in hitorwait.roads:
        if hitorwait.values[road]['r']:
            how_values[road] = hitorwait.values[road]['c']
            how_data.append(hitorwait.values[road]['c'])
            params.append(road)
    df = pd.DataFrame(columns=policies, index=params)
    df["Hit-or-Wait"] = how_data
    fig = plt.figure()
    ax = fig.add_subplot(111)
    width = 0.1
    df_how = df.ix[:,"Hit-or-Wait"]
    median_data = []
    for road in search_median.roads:
        if search_median.values[road]['r']:
            median_data.append(search_median.values[road]['c'])
            params.append(road)
    df["Median"] = median_data
    df_median = df.ix[:,"Median"]

    lowest_data = []
    for road in search_lowest.roads:
        if search_lowest.values[road]['r']:
            lowest_data.append(search_lowest.values[road]['c'])
            params.append(road)
    df["Lowest"] = lowest_data
    df_lowest = df.ix[:,"Lowest"]

    # opt_data = []
    # for road in search_opt.roads:
    #     if search_opt.values[road]['r']:
    #         opt_data.append(search_opt.values[road]['c'])
    #         params.append(road)
    # df["Optimal"] = opt_data
    # df_opt = df.ix[:,"Optimal"]


    print df_how
    print df_median

    ax = df.plot(kind="bar")
    ax.set_ylim([0,max(total_search_how)])

    title_string = 'Search Coverage:: Searching near %s.\nlost item is placed at %s'%(search_region,regions["roads"][lost_item_idx])
    plt.title(title_string)

    plt.tight_layout(h_pad=1)

    # df_how.plot.bar(color="blue", ax=ax, width=width, position=2, label="Hit-or-Wait")
    # df_median.plot.bar(color="green", ax=ax, width=width, position=2)
    # df_lowest.plot.bar(color="red", ax=ax, width=width, position=2)

    # plt.setp(plt.gca().get_xaxis().get_majorticklabels(), rotation=90)

    # leave room at the bottom

    df_lost_item = pd.DataFrame(index=["# searches","# of lost item found"])
    df_lost_item["hitorwait"] = [sum(total_search_how),sum(lost_item_how)]
    df_lost_item["median"] = [sum(total_search_median),sum(lost_item_median)]
    df_lost_item["lowest"] = [sum(total_search_lowest),sum(lost_item_lowest)]
    print df_lost_item
    ax2 = df_lost_item.plot(kind='bar')
    ax2.set_ylim([0,max(total_search_how)])


    # plt.figure()

    title_string = '# of searches and # items found:: Searching near %s.\nlost item is placed at %s'%(search_region,regions["roads"][lost_item_idx])
    plt.title(title_string)

    plt.tight_layout(h_pad=1)

    plt.show()


    # pprint.pprint(how_dict)
# save_data()
#notification policy
if __name__ == "__main__":
    # sys.argv[1] is lat, lon of search region
    # lat = float(sys.argv[1].strip().split(',')[0])
    # lon = float(sys.argv[2])

    #42.064409, -87.685577

    #42.045646, -87.699023
    #42.048299, -87.703014
    #42.040293, -87.670414
    #42.042389, -87.671388
    #42.058386, -87.677872 # noyes
    # lat,lon = 42.042389, -87.671388 # lake sheridan
    # 42.060312, -87.685111
    # 42.063697, -87.683695

    new_lat = random.uniform(BOTTOM, TOP)
    new_lon = random.uniform(LEFT, RIGHT)

    print new_lat,new_lon
    lat,lon = new_lat,new_lon

    # lat,lon = 42.05439705, -87.6763453693 #sheridan
    # lat,lon = 42.0334462206, -87.6752763353

    #### comment it out for randomly selecting a lost item location ####
    lat,lon = 42.058353, -87.677991

    simulate(lat,lon)

# evanston_dict = {}
# evanston_dict["results"] = evanston_list
# with open('evanston_list.json', 'w') as outfile:
#     json.dump(evanston_dict,outfile)

