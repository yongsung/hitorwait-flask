import numpy as np
import random

class HitorWait:
    def __init__(self, roads):
        self.searchPaths = []
        self.roads = roads
        self.itemFoundCount = 0
        self.values = {}

        for i in self.roads:
            # print i
            lookup = {'v': False, 'r': False, 'c': 0, 'l': False, 'value':1.0}
            # 'v' not visited, 'r' region to search, 'c' search count, 'l', lost item found
            self.values[i] = lookup

        # print self.roads
        # print self.values

    def addSearchRegionDown(self,region):
        for r in region:
            self.values[r]['r'] = True

    def markLostItem(self, road):
        self.values[road]['l'] = True

    def setSearchValue(self,state_dict):
        for key,value in state_dict.iteritems():
            self.values[key]['value'] = value
            print self.values[key]

    def setSearchCount(self,state_dict):
        for key,value in state_dict.iteritems():
            self.values[key]['c'] = value
            print self.values[key]

    def computeSearchValue(self, weighCounts):
        value = 0
        for i in self.roads:
            if self.values[i]['c']>0 and self.values[i]['r']:
                value += weighCounts(self.values[i]['c'])
        return value

    def expDecaySum(self, num):
        sum = 0
        for i in range(num+1):
            # print "printing num here: "
            # print num
            sum += self.expDecay(i)
        return sum

    def expDecay(self, num):
        return (1/2.0) ** num

    def getTotalSearchCounts(self):
        total = 0
        for i in self.roads:
            if self.values[i]['c']>0 and self.values[i]['r']:
                total += self.values[i]['c']
        return total

    def clearSearchProgress(self):
        for i in self.roads:
            self.values[i]['c'] = 0
        self.itemFoundCount = 0

    def printSearchProgress(self):
        for i in self.roads:
            if self.values[i]['c']!=0:
                print "search counts: %d at road %s" \
                    %(self.values[i]['c'],i)
            elif self.values[i]['r']:
                print 'not searched'
            else:
                print "not a search region"

    def printSearchCoverage(self):
        for i in self.roads:
            if self.values[i]['r']:
                print "road segment: %s's search count is %d" % (i, self.values[i]['c'])
    def getSearchCount(self):
        roadcount = {}
        for i in self.roads:
            if self.values[i]['c']!=0:
                # print "search counts: %d at road %s" \
                    # %(self.values[i]['c'],i)
                roadcount[i] = self.values[i]['c']
            elif self.values[i]['r']:
                roadcount[i] = 0
            else:
                roadcount[i] = -1
        return roadcount

    def computeWaitingDecisionTable(self, roadPredictModel, maxTime):
        try:
            knownValues = []
            knownDecisions = []
            for t in range(0, maxTime):
                knownValues.append({})
                knownDecisions.append({})

            for i in self.roads:
                if self.values[i]['r']:
                    # knownValues[0][i] = self.values[i]['value']
                    knownValues[0][i] = self.expDecay(self.values[i]['c'])
                    knownDecisions[0][i] = "Hit"
                else:
                    knownValues[0][i] = 0
                    knownDecisions[0][i] = "Wait"

            for t in range(1, maxTime):
                for state in self.roads:
                    # print "state is %s" %state

                    # based value is 1 if it is marked as search region, otherwise 0.
                    if self.values[state]['r']:
                        # knownValues[t][state] = self.values[state]['value']
                        knownValues[t][state] = self.expDecay(self.values[state]['c'])
                        knownDecisions[t][state] = "Hit"

                    else:
                        knownValues[t][state] = 0
                        knownDecisions[t][state] = "Wait"

                    # base decision is always hit

                    # see if waiting is better
                    expectedValue = 0;

                    ######## comment out below for adding direction ####
                    curr = {}
                    if state in roadPredictModel:
                        curr = roadPredictModel[state]

                    if curr.keys()>0:
                        next_nodes = curr.keys()
                        # print state
                        for next in next_nodes:
                            # print next
                            if next in self.roads:
                            # print knownValues
                                print next
                    ####################################################
                                expectedValue += roadPredictModel[state][next] * knownValues[t-1][next]

                                print "expected value: %f and knownValues %f at step %d" \
                                    %(expectedValue,knownValues[t-1][next],t)
                                if expectedValue > knownValues[t][state]:
                                    # decision is to WAIT
                                    knownValues[t][state] = expectedValue
                                    knownDecisions[t][state] = "Wait"
            return {'values': knownValues, 'decisions': knownDecisions}
        except Exception as error:
            print "in compute waiting decision table"
            print "error message is: %s"%error

    # DELETE THIS FUNC LATER. FOR DEBUGGING
    def addSearches(self,search_dict):
        for k,v in search_dict.iteritems():
            self.values[k]['c'] = v
        print self.values

    def requestSearchMedian(self, userIntersectionLists):
        median = self.getMedian()
        for i in range(0,len(userIntersectionLists)):
            currentRoad = userIntersectionLists[i]
            if currentRoad not in self.roads:
                # print currentIntersection
                continue
            if self.values[currentRoad]['c'] <= median:
                self.values[currentRoad]['c'] +=1
                # print "searching for lost item at %s" % currentRoad
                if self.values[currentRoad]['l']:
                    self.itemFoundCount += 1
                break

    def getMedian(self):
        values = []
        for i in self.roads:
            if self.values[i]['r']:
                values.append(self.values[i]['c'])
        # print np.median(values)
        return np.median(values)

    def getMinimum(self):
        values = []
        for i in self.roads:
            if self.values[i]['r']:
                values.append(self.values[i]['c'])
        # print values,np.min(values)
        return np.min(values)

    def requestSearchLowest(self,userIntersectionLists):
        lowest = self.getMinimum()
        for i in range(0,len(userIntersectionLists)):
            currentRoad = userIntersectionLists[i]
            if currentRoad not in self.roads:
                # print currentIntersection
                continue
            if self.values[currentRoad]['c'] <= lowest:
                self.values[currentRoad]['c'] +=1
                # print "searching for lost item at %s" % currentRoad
                if self.values[currentRoad]['l']:
                    self.itemFoundCount += 1
                break

    def requestSearchOPT(self,userIntersectionLists):
        lowest = self.getMinimum()
        candidates = {}
        candidates_lowest = {}
        idx = ""
        for i in range(0,len(userIntersectionLists)):
            currentRoad = userIntersectionLists[i]
            if currentRoad not in self.roads:
                # print currentIntersection
                continue
            else:
                candidates[currentRoad] = self.values[currentRoad]

        for k,v in candidates.iteritems():
            if v['c']<=lowest:
                candidates_lowest[k] = v['c']
        # print candidates_lowest
        if len(candidates_lowest.keys()) != 0:
            idx = random.choice(candidates_lowest.keys())
            # print idx, candidates_lowest[idx]
            self.values[idx]['c']+=1
            if self.values[idx]['l']:
                self.itemFoundCount += 1

            # print "searching for lost item at %s" % idx

    def requestSearchHitorWait(self, path, decisions):
        """
        INPUT: current road
        ALG: Hit-or-Wait Decision Theoretic Alg to compute
             best decision (to hit or wait) as a person walks
             through the region
        Hyp: this is the correct decision-theoretic solution
        """
        maxTime = len(decisions['decisions'])
        # print "maxTime is %d" % maxTime

        for (index,state) in enumerate(path):
            # print state, index, maxTime-index-1
            if index >= maxTime:
                break
            if state not in self.values:
                continue
            if self.values[state]['r'] and decisions['decisions'][maxTime-index-1][state] == "Hit":
                self.values[state]['c'] +=1

                # print "searching for lost item at %s" % state
                # print ".. at index %s and decision point %s" % (index, maxTime-index-1)
                # return (state,maxTime-index-1)
                if self.values[state]['l']:
                    self.itemFoundCount += 1
                break


