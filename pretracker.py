from __future__ import division
import os
import time
from collections import defaultdict, Counter
from sets import Set
import urllib2
import json

from bson.son import SON
from bson.objectid import ObjectId
import math
from math import radians,cos,sin,asin,sqrt,atan

import random
from numpy import sum
import re
import pprint

import pymongo
from pymongo import MongoClient, GEO2D
from ykUtils import calculate_bearing, calculateNewLatLng

from roadAPI import haversine,get_direction,getRoad,getMovementModel,getRoadCoordinates

import io
import reverse_geocoder as rg

from flask import Flask, render_template, jsonify, request, Response

from how import HitorWait

from apns import APNs, Frame, Payload

app = Flask(__name__)
app.config.from_object('config')

print app.config['DEBUG']

if app.config['DEBUG']:
    ## localhost
    uri = "mongodb://localhost:27017"
    #for app
    dbName = "how"
else:
    # !!need to put dbname in the mongdb uri!!
    uri = "mongodb://yk:kim@ds011389.mlab.com:11389/otg"
    dbName = "otg"

#for simluation
if app.config['SIMULATION']:
    dbName = "simulation"

print uri, dbName

client = MongoClient(uri)
db = client[dbName]

in_the_region = False

def check_same_trip(user,lat,lon,date):
    # get last locations
    locations = db.locations

    query = {"user":user}
    res = locations.find(query).sort("_id",-1).limit(1)
    print "%d trips for user %s"%(res.count(),user)
    if res.count()>0:
        for r in res:
            lat0,lon0 = r["coordinates"][-1][0],r["coordinates"][-1][1]
            distance = haversine(lat0,lon0,lat,lon)
            print "distance to previous location is %d"%distance
            time_diff = (date - r["lastModified"])
            print "time elapsed: %d"%time_diff
            # maybe we should just use distance
            if distance >= 35.0:
                if time_diff <= 60.0*10:
                    new_coord = r["coordinates"]
                    new_coord.append([lat,lon])
                    print new_coord
                    print "adding to existing trip"
                    # dbLocations.update({"_id":ObjectId(r["_id"])},{"$set":{"coordinates":new_coord},"$currentDate": { "lastModified": True}})
                    locations.update({"_id":ObjectId(r["_id"])},{"$set":{"coordinates":new_coord,"lastModified":date}})
                    updateRoutes(r["_id"],r["user"],new_coord)
                else:
                    print "last location update came after x mins, starting a new trip"
                    # need the last location update. and store from there
                    uid = None
                    coord = []
                    coord.append([lat,lon])
                    content = {"coordinates":coord,"user":user,"lastModified":date,"created":date}
                    uid = locations.insert(content)
                    if uid != None:
                        print uid
                        updateRoutes(uid,r["user"],coord)
                    else:
                        print "error"
    else:
        uid = None
        coord = []
        coord.append([lat,lon])
        content = {"coordinates":coord,"user":user,"lastModified":date,"created":date}
        uid = locations.insert(content)
        if uid != None:
            print uid
            updateRoutes(uid,user,coord)
        else:
            print "error"

        # whenever there is a location update --> update routes
        # if distance is >= 50.0 and time <= 5 mins
        # need updated_time ,created_time
        # regions.update({"_id":uid},{"$set":{"nearby_regions":tmp_regions}})

def updateRoutes(loc_id,user,coord):
    # TODO
    # add locations
    # if it is updating existing one, we should update the existing routes
    # else create a new one.
    print "update routes %s"%loc_id

    locations = db.locations
    routes = db.routes

    uid = ""
    query_result = routes.find({"loc_id":loc_id})
    print query_result.count()

    roads = []

    if query_result.count()>0:
        #update existing one.
        for r in query_result:
            uid = r["_id"]
            res = getRoad(coord)
            for r in res:
                roads.append(r["name"])
            directions = get_direction(coord)
            routes.update({"_id":uid},{"$set":{"roads":roads,"directions":directions, "coordinates": coord}})
    else:
        #create new routes
        res = getRoad(coord)
        for r in res:
            roads.append(r["name"])
        directions = get_direction(coord)

        road_dict = defaultdict(list)

        road_dict["roads"]= roads
        road_dict["directions"] = directions
        road_dict["coordinates"] = coord
        road_dict["user"] = user
        road_dict["loc_id"] = loc_id
        road_dict["created"] = int(time.time())
        road_dict["lastModified"] = int(time.time())
        uid = routes.insert_one(road_dict)
        print uid

    latest_coordinate = coord[-1]
    latest_road = roads[-1]
    print "road is: %s and user is %s"%(latest_road, user)
    get_search_region(user,latest_road,latest_coordinate[0],latest_coordinate[1])

def get_search_region(user,road,lat,lon):
    regions = db.regions
    # in meters
    dist = 300
    #42.049423, -87.678085
    query_str = {"loc":SON({"$near":{"$geometry":{"type":"Point", "coordinates":[lon,lat]},"$maxDistance":dist}})}
    result = regions.find(query_str).limit(1)
# # result = regions.find({"_id":ObjectId("58965dd58a676737d6318587")})
    if result.count() > 0:
        in_the_region = True
        for r in result:
            print r
            hitorwait(user,road,r["_id"],r["nearby_regions"],r["region_coordinates"])

def hitorwait(user,road,search_region_id,nearby_regions, region_coordinates):
    # calculate 3 steps in total. 2 steps look ahead
    steps = 5
    # print road, user
    '''
    getMovementModel(user,currentRoad,steps,modelType,hasHeading):
    '''
    models = None
    model_type = "indiv"
    models = getMovementModel(user,road,steps,model_type,0)

    if len(models.keys())==0:
        print "=== individual movement model is empty ==="
        print "=== generating population-based movement model is empty ==="
        models = getMovementModel(user,road,steps,"pop",0)
        model_type = "pop"
    # pprint.pprint(dict(models))
    #TODO:
    # if model is empty, use population-based model

    roads = []
    roadsCoordinates = region_coordinates

    # print roadsCoordinates
    # pprint.pprint(dict(models))

    # should get road coordinates from the search region.
    # if it is population-based
    for i,v in models.iteritems():
        if i not in roads:
            roads.append(i)

            # if it is population-based getRoadCoordinates from
            key,value = getRoadCoordinates(i, user, model_type)
            roadsCoordinates[key] = value
        for j in v.keys():
            if j not in roads:
                roads.append(j)
                key,value = getRoadCoordinates(j,user, model_type)
                roadsCoordinates[key] = value

    values = {}

    road_list = roads + nearby_regions.keys()

    hitorwait = HitorWait(road_list)
    hitorwait.addSearchRegionDown(roads)
    hitorwait.setSearchCount(nearby_regions)

    decisions = hitorwait.computeWaitingDecisionTable(models,steps)
    # need all the coordinates here.
    decisions.update({"coordinates":roadsCoordinates})
    decisions.update({"models":models})
    # pprint.pprint(decisions)

    # store the decision to the user document.
    users = db.users
    users.update({"user":user},{"$set":{"decision":decisions,"hasDecision":True}})

    # sendDecision(user,decisions,search_region_id)

def sendDecision(user,decisions,search_region_id):
    users = db.users
    result = users.find({"user":user})
    for r in result:
        if "decision" in r:
            print "sending notification to: %s"%r["tokenId"]
            print r["decision"]
            apns = APNs(use_sandbox=True, cert_file='devCert.pem', key_file='devKey.pem')
            token_hex = r["tokenId"]
            # we can only send 4kb. so in the decisions, we only send coordinates for hit roads.
            payload = Payload(alert="Hit-or-wait decision is here!",content_available=1,custom={"coordinates":r["decision"]["coordinates"],"decisions":r["decision"]["decisions"],"models":r["decision"]["models"], "region_id":search_region_id})
            apns.gateway_server.send_notification(token_hex, payload)

# sendDecision("yk",0,"58dc30618eae046ecabea47c")

# res = db.regions.find().sort("_id",-1).limit(1)
# for r in res:
#     roads = r["nearby_regions"]
#     coords = r["roads_coordinates"]
#     # hitorwait("yk","605 Garrett Place",r["_id"],roads, coords)
#     hitorwait("yk","605 Garrett Place","58e0225c8dda179cef52b4f2",roads,coords)

# timestamp = int(time.time())
# check_same_trip("yk",42.06932445,-87.1187664,timestamp)

# uid = ObjectId("58d2e6efaa41131dbb2afeb6")
# updateRoutes(uid)

# client = MongoClient(uri)
# db = client[dbName]
# regions = db.regions

# res = regions.find({"_id":uid})
# for r in res:
#     tmp = r["nearby_regions"]
#     tmp["1620 Chicago Avenue"] = 5
#     tmp["1627 Chicago Avenue"] = 8
#     tmp["1635 Chicago Avenue"] = 0
#     print tmp
#     regions.update({"_id":uid},{"$set":{"nearby_regions":tmp}})





