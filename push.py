import time
from flask import Flask, render_template, jsonify, request, Response
from pymongo import MongoClient, GEO2D

from apns import APNs, Frame, Payload

import schedule
app = Flask(__name__)
app.config.from_object('config')

if app.config['DEBUG']:
    ## localhost
    uri = "mongodb://localhost:27017"
    #for app
    dbName = "how"
else:
    # !!need to put dbname in the mongdb uri!!
    uri = "mongodb://yk:kim@ds011389.mlab.com:11389/otg"
    dbName = "otg"

client = MongoClient(uri)
db = client[dbName]

def sendPush():

    users = db.users
    result = users.find()
    for r in result:
        token = r["tokenId"]
        apns = APNs(use_sandbox=False, cert_file='otgCert.pem', key_file='otgKey.pem')
        token_hex = token
        payload = Payload(content_available=1,custom={"custom":"item"})
        # payload = Payload(content_available=1)
        apns.gateway_server.send_notification(token_hex, payload)

def run_schedule():
    while 1:
        schedule.run_pending()
        time.sleep(1)

def scheduler():
    # sendPush()
    print "scheduler run every 0.5 minutes"
    schedule.every(30).seconds.do(sendPush)
    run_schedule()
