from __future__ import division
import os
import time
from collections import defaultdict, Counter
from sets import Set
import urllib2
import json

from bson.son import SON
from bson.objectid import ObjectId
import math
from math import radians,cos,sin,asin,sqrt,atan

import random
from numpy import sum
import re
import pprint

import pymongo
from pymongo import MongoClient, GEO2D
from ykUtils import calculate_bearing, calculateNewLatLng

import io
import reverse_geocoder as rg

from flask import Flask, render_template, jsonify, request, Response

app = Flask(__name__)
app.config.from_object('config')
print app.config['DEBUG']

if app.config['DEBUG']:
    ## localhost
    uri = "mongodb://localhost:27017"
    #for app
    dbName = "how"
else:
    # !!need to put dbname in the mongdb uri!!
    uri = "mongodb://yk:kim@ds011389.mlab.com:11389/otg"
    dbName = "otg"

#for simluation
if app.config['SIMULATION']:
    dbName = "simulation"

client = MongoClient(uri)
db = client[dbName]

geocoder = rg.RGeocoder(mode=2, verbose=True, stream=io.open('chicago_all_street.csv',encoding='utf-8'))

## Heroku MONGODB URI
# uri = "mongodb://heroku_vv24r96s:h3poed8rejc1jojnco6jv9s68t@ds151018.mlab.com"
# port = 51018
# db = "heroku_vv24r96s"


# from googleRoadsAPI import nearest_road, get_road_segments_at_intersection
# from ykUtils import get_direction,calculate_bearing
# from itemSearch import HitorWait
# from intersection import RoadAPI

#TODO: put these credentials to config file.

#yongsung7
#old road api key: AIzaSyDCAxUMegcoZ0WK1mzMHMXOnVpoOcQ9wQA
#old place api key: "AIzaSyBibyshME7mPknwiFrNDpSbhTePHevRChk"
#geocoding api key: AIzaSyCZ4sHdWJ7fl-o0aHA1k_k5eFxO60lOOi4

#nudeltayk
#new road api key: AIzaSyCX5NZOvbM5_LoGMjLTxvjB3e6Z_VSmI0I
#new place api key: AIzaSyCuWdIv9-rpHJ85ojWVR3cfO3IzfjHMTww

#yk
#road api key: AIzaSyAqy1ojdaJQ7NVCpFS8cjcRzW40MrYgGag
#place api key: AIzaSyCPn7QugPSzQssnNz8Na4l5VROmpY98MgU

ROAD_API_KEY = "AIzaSyCX5NZOvbM5_LoGMjLTxvjB3e6Z_VSmI0I"
PLACE_API_KEY = "AIzaSyCuWdIv9-rpHJ85ojWVR3cfO3IzfjHMTww"
SNAP_TO_ROAD_API_KEY = "AIzaSyC80d0Y6qTYj-P2Lxh2xcBstzRRV3E4yVk"
GEO_API_KEY = "AIzaSyCZ4sHdWJ7fl-o0aHA1k_k5eFxO60lOOi4"


def nearest_road(datapoints):
    try:
        url_str = "https://roads.googleapis.com/v1/nearestRoads?points=%s&key=%s" % (datapoints,ROAD_API_KEY)
        url = urllib2.urlopen(url_str)
        html = url.read()
        json_data = json.loads(html)["snappedPoints"]

        originalPlaceId = ""
        originalIndexId = ""

        roads = []

        for i in json_data:
            placeId = i["placeId"]
            indexId = i["originalIndex"]
            if originalIndexId != indexId:
                originalIndexId = indexId

                place_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=%s&key=%s" % (placeId,PLACE_API_KEY)
                place_response = urllib2.urlopen(place_url).read()
                place_json_data = json.loads(place_response)

                road_name = place_json_data["result"]["name"]
                roads.append(road_name)

        return roads
    except Exception as error:
        print error

def get_direction(routes):
    route_list = []
    try:
        for index in range(len(routes)):
            if index+1<len(routes):
                coord = routes[index]
                coord2 = routes[index+1]

                direction =  calculate_bearing(float(coord[0]),float(coord[1]),float(coord2[0]), float(coord2[1]))

                action = ""

                if direction>=0 and direction<=45 or direction>=315 and direction <=360:
                    action =  "North"
                elif direction>45 and direction<=135:
                    action =  "East"
                elif direction>225 and direction < 315:
                    action = "West"
                elif direction>135 and direction<=225:
                    action = "South"

                route_list.append((action,direction))

            else:
                route_list.append(("End","End"))

        return route_list
    except Exception as error:
        print error

def haversine(lat1, lon1, lat2, lon2):
    diffLong = math.radians(lon2 - lon1)

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(math.radians(lat1)) * math.sin(math.radians(lat2)) - (math.sin(math.radians(lat1))
            * math.cos(math.radians(lat2)) * math.cos(diffLong))

    initial_bearing = math.atan2(x, y)

    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360

    radius = 6371 # km
    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c

    # print d

    # returning in meters
    return d*1000.0

def toRoads(routes):
    datapoints = ""
    road_dict = defaultdict(list)
    try:
        json_data = routes
        road_dict["coordinates"]=json_data["coordinates"]

        out_str = ""
        cnt = 0
        print road_dict["coordinates"]
        for r in road_dict["coordinates"]:
            if cnt < len(road_dict["coordinates"])-1:
                out_str+= '%f,%f'%(r[0],r[1]) + "|"
            else:
                out_str+= '%f,%f'%(r[0],r[1])
            cnt+=1
        print out_str
        road_segs = nearest_road(out_str)
        # road_segs = [u'20801-20855 Valley Green Dr', u'Infinite Loop', u'Infinite Loop', u'Infinite Loop', u'Infinite Loop', u'Infinite Loop', u'Infinite Loop', u'Infinite Loop', u'Infinite Loop']
        return road_segs
    except Exception as error:
        print error


def getRoutes(user, routeId):
    client = MongoClient(uri)
    db = client[dbName]
    locations = db.locations
    locs = locations.find({"user":user})
    # print locs[0]

    return locs
    # toRoads(locs[0])
    # get_road_segment(query_result)

#TODO: change toRoads to getRoad and change the coordinates
def postRoutesToDB(user,routeId):
    client = MongoClient(uri)
    db = client[dbName]
    locations = db.locations
    routes = db.routes

    try:
        locs = locations.find({"routeId":routeId})
        # print locs[0]
        roads = []
        result = getRoad(locs[0]["coordinates"])

        for r in result:
            roads.append(r["name"])
        directions = get_direction(locs[0]["coordinates"])

        road_dict = defaultdict(list)

        road_dict["roads"]= roads
        road_dict["directions"] = directions
        road_dict["coordinates"] = locs[0]["coordinates"]
        road_dict["user"] = locs[0]["user"]
        road_dict["routeId"] = locs[0]["routeId"]

        # print road_dict
        road_id = routes.insert_one(road_dict)

        print road_id
    except Exception as error:
        print "postRoutesToDB"
        print error

def get_prediction(model,num):
    new_dict = defaultdict(dict)
    for i,v in model.iteritems():
        tmp = dict(Counter(v))
        tmp_cnt = 0
        for k in tmp.keys():
            tmp_cnt+=tmp[k]
            # print tmp_cnt,
        # print tmp_cnt
        tmp.update((x, y/tmp_cnt) for x, y in tmp.iteritems())
        new_dict[i] = tmp
    # pprint.pprint(dict(new_dict))
    return new_dict

#TODO: need a helper function to get rid of trips with noisy data.
def getMovementModel(user,currentRoad,steps,modelType,hasHeading):
    '''
    this is a function for generating movement model
    given a current road, steps to look ahead, modelType (either individual- or population-based),
    whether or not include heading (0 or 1)
    output, movement model
    '''

    routes = db.routes
    tmp_regex = r'%s'%currentRoad
    regex = re.compile(tmp_regex)
    query = {}
    if modelType == "indiv":
        query = {"roads":regex,"user":user}
    else:
        query = {"roads":regex}

    query_result = routes.find(query)

    cnt = 0
    roadList = []
    directionList = []

    for doc in query_result:
        roads = doc["roads"]
        directions = doc["directions"]
        roadList.append(roads)
        directionList.append(directions)

    heading = ""

    # zero index
    steps = steps - 1

    model = defaultdict(list)
    num_routes = len(roadList)
    initial_heading = ""
    initial_road = ""

    for idx in range(len(roadList)):
        added = False
        if currentRoad in roadList[idx]:
            curr_idx = roadList[idx].index(currentRoad)
            # print "current road is %s at index %d" % (currentRoad, curr_idx)

            initial_heading = directionList[idx][curr_idx-1][0]
            initial_road = roadList[idx][curr_idx-1]

            # this does not fail even when the trip don't have enough steps ahead.
            # it keeps adding to the model until it reaches the last road.
            for j in range(curr_idx,curr_idx+steps):
                # check if the index is out of bound
                if j+1 < len(roadList[idx]):
                    heading = directionList[idx][j][0]
                    if roadList[idx][j] != roadList[idx][j+1]:
                        # include road segment and heading for the model
                        if hasHeading == 1:
                            model[(roadList[idx][j],heading)].append((roadList[idx][j+1],heading))

                        # only include road segment for the model.
                        else:
                            model[roadList[idx][j]].append(roadList[idx][j+1])
                        # if this trip history contributed to the model, set added = True
                        # actually we don't need this..
                        added = True
        if added:
            # total # trips contributed to the model.
            cnt += 1
        # if cnt > 10:
        #     break

    num_routes = cnt
    # print "initial road is %s and heading is: %s" % (initial_road, initial_heading)
    print num_routes, cnt
    # pprint.pprint(dict(model))
    movement_model = get_prediction(model,num_routes)
    pprint.pprint(dict(movement_model))
    return movement_model

def getRoadCoordinates(road,user,modelType):
    dbRoutes = db.routes
    query = None

    if modelType == "indiv":
        query = {"roads":road, "user":user}
    else:
        query = {"roads":road}
    res = dbRoutes.find(query).limit(1)
    # print res.count()
    for r in res:
        # print r["_id"]
        roads = r["roads"]
        coordinates = r["coordinates"]
        idx = roads.index(road)
        # print idx
        # print roads[idx]
        # print coordinates[idx]
        return (roads[idx], coordinates[idx])

def getNearbyCoordinates(lat,lng,bearing,distance):
    bearing = bearing
    dist = distance
    # if bearing == 90 --> 89.5
    # bearing == 180 -> 179.1
    """
    if  == 0: # north
        bearing = 0.0
    elif  == 90: # east
        bearing = 89.5
    elif  == 180: # south
        bearing = 179.1
    else: == ?# west
        bearing = 268.6
    """

    #TODO: need to get street bearings for calculating this!
    bearing1 = 89.5
    bearing2 = 179.1
    # lookahead = 1

    steps = 20
    # print bearing1, bearing2, intersection_lat,intersection_lng
    coordinates = []
    for lookahead in range(1,steps+1):
        # print lookahead
        coord = {}
        #east
        eastCoord = calculateNewLatLng(lat,lng,bearing1,dist*lookahead)
        #west
        westCoord = calculateNewLatLng(lat,lng,bearing1+2*bearing1,dist*lookahead)
        #south
        southCoord = calculateNewLatLng(lat,lng,bearing2,dist*lookahead)
        #north
        northCoord = calculateNewLatLng(lat,lng,bearing2-bearing2,dist*lookahead)

        coord["east"] = eastCoord
        coord["west"] = westCoord
        coord["south"] = southCoord
        coord["north"] = northCoord

        coordinates.append(eastCoord)
        coordinates.append(westCoord)
        coordinates.append(southCoord)
        coordinates.append(northCoord)
    # print coordinates
    return coordinates

def get_nearby_roads(lat,lon):
    # 42.058372, -87.678015
    # print getRoad([(42.058372, -87.678015)])
    dist = 0.005
    coords = getNearbyCoordinates(lat,lon,0,dist)

    result = getRoad(coords)
    roads = []
    coordinates = []
    for r in result:
        # print r["name"], r["lat"], r["lon"]
        lat,lon = float(r["lat"]),float(r["lon"])
        if r["name"] not in roads:
            roads.append(r["name"])
            coordinates.append([lat,lon])
    # print roads
    # print coordinates

    regions = {}
    search_region = getRoad([(lat, lon)])[0]
    regions["coordinate"] = (lat, lon)
    regions["name"] = search_region["name"]
    regions["roads"] = roads
    regions["roads_coordinates"] = coordinates

    return regions

def addNearbyRoadstoRegion(nearby_regions):
    client = MongoClient(uri)
    db = client[dbName]
    regions = db.regions
    rid = db.regions.insert(regions)
    print


def getRoad(coord):
    coord = coord
    result = geocoder.query(coord)
    roads = []
    # for r in result:
    #     roads.append(r["name"])
    # print result
    return result

# getMovementModel("fowebb","2003 Sheridan Road",10,"idv")
# getMovementModel("fowebb","2003 Sheridan Road",10,"popu")

# client = MongoClient(uri)
# db = client[dbName]
# regions = db.regions
# road = "618 Church Street"
# key = "nearby_regions.%s"%road
# query_str = {key:{"$exists":"true"}}
# #42.049423, -87.678085
# # query_str = {"loc":SON({"$near":{"$geometry":{"type":"Point", "coordinates":[-87.678085,42.049423]},"$maxDistance":1000}})}

# result = regions.find(query_str)
# for r in result:
#     print r
#     print r["_id"]

# regions.update({"_id":ObjectId("58965dd58a676737d6318587")},{"$set":{"1617 Chicago Avenue":1}})




