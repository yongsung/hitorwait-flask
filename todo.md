# TODO and hq feedback

#yk TODO items
## upcoming deadlines:
- CSCW: Mid May
- HCOMP: Late May or Early June
- UIST: April 4th
- CHI: Mid September
## Feb 4, 2017
1. uploading trips when there is no location update for x minutes. [done]
2.

## Jan 31,2017
1.
2.

## Jan 18,2017
1. handle corner cases there is no previous route history, steps lookahead overflow
2. show # of missed opportunities
3. how to make visualization
4. need a frontend working with backend
    - tune locationCounter in Pretracker for uploading locations
    - check hit-or-wait is working with background location updates, pre-tracking notification,
    - connect with regions model
    - record searches
    - visually show search counts
    - change notification message

## Jan 10, 2017
1. should be able to add/update search regions.
2. implement backend with no geocoding limits.
    - https://github.com/yongsung7/reverse-geocoder (for reverse geocodoing)
    - get dataset from https://openaddresses.io/
        - problem: many data is missing.
        - [GIS data from Evanston](http://data.evanston.opendata.arcgis.com/datasets/6d0b7733d4ee475ca8028b876ba6e605_4)
            - also some data is missing!
    - we can also use openstreetmap data and GISgraphy. Both opensource
        - [Ways to convert OSM data to csv](https://wiki.openstreetmap.org/wiki/Osmconvert#Writing_CSV_Files)
    - threads: https://news.ycombinator.com/item?id=9281222
    - https://wiki.openstreetmap.org/wiki/Nominatim (policy limits)
        - can only install on linux
    - https://github.com/brianw/osmgeocode
    - convert HEXEWKB to latitude, longitude
        - [using shapely](http://stackoverflow.com/questions/34270800/how-to-convert-hexewkb-to-latitude-longitude-in-python)
        - [another approach](http://gis.stackexchange.com/questions/78838/how-to-convert-projected-coordinates-to-lat-lon-using-python)
3. should add possible roads to a region.
4. handle cases where there is no previous route history, or currentRoadIndex+steps > len(routes[])
5. need to update getNearbyCoordinates func
6. adding context into the model: datetime, destination?, speed (can infer transit mode)


## Dec 14, 2016
### during the break
#### hit-or-wait
1. a function that calculates value of searches
    - how to calculate search value based on prob(others reaching and searching)?
    - references?
2. handle cases in which there are multiple large search regions.
    - how should we compute? we need better model to calculate transition probability. because accuracy of markov model with many steps lookahead is low.
3. design better ways to query and cache (lat,lng) pair -> road segments
    - possible options: Google Road API, MapBox (probably should send an email to Hecht)
    - ways to reduce # request:
        - check every 40-50 meters.
        - is there any way to get the centroid lat,lng of road segment?
            - if so, we can do the local look up (~30 meters)
4. dynamically show this process --> take a look at how eric horvitz is demonstrating AI algorithms.
5. what are the contributions?
6. what is the task pick-up model?

#### learning
1. AI, Algo, Swift, Meteor ==> Crowdfound app with meteor as backend with hit-or-wait algo
    - AI: MDP, Probability, Bayes net
        - AIMA book
    - Decision theory: good curriculum or book? Horvitz papers.
        - [Computational models of reasoning & action](http://research.microsoft.com/en-us/um/people/horvitz/computational_rationality_readings.htm)
        - [People & machines](http://research.microsoft.com/en-us/um/people/horvitz/people_machines_readings.htm)
    - Algo: read chapters suggested by algo part I on Coursera + [dynamic programming from MIT](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/)
        - clrs algo book.
    - Swift: swift lang guide + stanford lectures
        - lost and found app basic functionality + social feature.
    - backend should be able to easily change between different task notification approaches.
2. go through writing lectures! pafper submission period is coming!
    - [Stanford SciWriting](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/lecture-videos/)
    - read Element of Style again

### Winter and Spring quarter
1. design principles for otg crowdsourcing systems. (it would be nice to have both crowdfound and libero. change the name!)
    - how one should design otg systems? what are the important features?
        - notification is apparent one important feature. what are some desirable social interactions? and how could we support those interactions among people?
    - how does this differ from existing payment-per-task systems?
2. experiment with hit-or-wait algorithm
    - can this be a standalone paper? or better be a technical contribution of otg systems paper?
    - currently, we have 5989 running routes collected
        - todo: check how many lat,lng pairs we have.
            - (assume we have 20-30 road segments, we have around 180,000 road segments for experiment)
    - how many unique users?
3. need more context-awareness
    - destination, mode of transportation, weather, datetime, situational factors.
    - to start with, implement models for cost of disruption, cost of diversion, situational factors
4. start minor MTS qual. Send an email to aaron

## Dec 9, 2016
1. the way I implemented movement model was totally wrong!!
    - Because I assumed that every other states are reachable from the current state.
    - Also, i did not calculate prob(next states of next state|next state)!!!
2. use road segment and direction as state
3. need a function that can assign search value for testing.
    - what should be a general function for calculating search value?
4.
10. definitely need to clean up the code a little bit and merge to master branch

## Nov 29, 2016
1. put direction!!!!
2. need to handle unnamed road
3. how to deal with google road and place api query limit
4. implement naive node counting or geofence
5. calculate search values
6. write down test cases
7. show decision for each person at the search regions and search value
8. dynamically show this process --> take a look at how eric horvitz is demonstrating AI algorithms.
    - for animation: https://jakevdp.github.io/blog/2013/06/01/ipython-notebook-javascript-python-communication/
    - http://jakevdp.github.io/blog/2013/05/12/embedding-matplotlib-animations/
## Nov 17, 2016

1. get next road probability
    * take as input all the roads at the intersection
    * show the probability based on previous road sequence
2. need a boundary for search region
    * currently it's just marking search region
3. think about what does steps, terminal state mean?
    * can we just use # of intersections in the large search region and use it as steps?
4. can we improve value of search so that it is more than # of search counts?
5. should take into account directionality; think about how many steps look ahead
    * maybe this point can be resolved when 1) is resolved.


#hq feedback
## Dec 14, 2016
1. viz and test cases to simluate and test
2. system running with frontend and backend
3. simulations
    - 1) simulated test cases
    - 2) real-world proof-of-concept
    - 3) simulated experiemtn
    - 4) field deployment

## Nov 30, 2016
1. what were the feedback? [PUT IT HERE]

## Nov 16, 2016
1. essentially what we need is to decide go straight, left, or right at next intersection
2. in each road segment, value representation can be a distribution
3. what is the right model for the value of searches
    * taking into account who might potentially show up in the future
    * need more reference for V* - V(-i)*

## July, 2016
* need a list for search count on each street
* check boosting algorithm
* need to refine the model
* what happens if someone is staying in the same road?
    * yk: maybe check every 50 meters; keep the duplicate roads
    * still nice to keep the abstraction of hit-or-wait
        - why implementing for road specific scenario?

# yk notes:
* need better job on deciding which intersections for searches
* how do you wanna show the state?
    * what is the terminal state?
    * use intersections as state
        * how do you get intersections as state? --> what are the intersections that it has in the lost item region?
        * if a user come across the intersection we pull out the decision table
        * assume a user is in the search region
        * for each intersection in the search region, we decide whether to ping on the street the person is going or not
        * how many steps look ahead? one state?
