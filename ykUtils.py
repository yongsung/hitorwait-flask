import json

import datetime
import random
import math
from math import radians,cos,sin,asin,sqrt,atan

import os
from collections import defaultdict
import pprint
from itertools import izip

import pymongo
from pymongo import MongoClient

# import pandas as pd

def calculateNewLatLng(lat,lng,bearing,distance):
    R = 6371 # km
    d = distance
    lat1 = math.radians(lat) #Current lat point converted to radians
    lon1 = math.radians(lng) #Current long point converted to radians

    lat2 = math.asin( math.sin(lat1)*math.cos(d/R) +
         math.cos(lat1)*math.sin(d/R)*math.cos(bearing))

    lon2 = lon1 + math.atan2(math.sin(bearing)*math.sin(d/R)*math.cos(lat1),
                 math.cos(d/R)-math.sin(lat1)*math.sin(lat2))

    lat2 = math.degrees(lat2)
    lon2 = math.degrees(lon2)

    return (lat2,lon2)

def calculate_bearing(lat1,lng1,lat2,lng2):
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)

    diffLong = math.radians(lng2 - lng1)

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1)
            * math.cos(lat2) * math.cos(diffLong))

    initial_bearing = math.atan2(x, y)

    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360
    return compass_bearing

def gpx():
    client = MongoClient('localhost',27017)
    db = client.myDB
    query = {"user":"103682706"}
    q_result = db.locations.find(query)

    cnt = 0
    header = "<gpx>\n"
    header_end = "</gpx>"
    for q in q_result:
        coords = q["coordinates"]
        out_str = ""
        file_name = "103682706_%d.gpx"%cnt
        out_str += header
        for coord in coords:
            lat = coord[0]
            lng = coord[1]

            waypoint = '<wpt lat="%f" lon="%f"></wpt>\n' % (lat,lng)
            out_str += waypoint
        out_str += header_end
        with open(file_name,'w') as file:
            file.write(out_str)
        cnt +=1
        break

# gpx()


